package zeromq.chapter2.handinginterrupt;

import org.zeromq.ZMQ;
import org.zeromq.ZMQException;

public class HandingInterrupt {
	
	public static void main(String[] args) {
		
		// Prepare our context and socket
		final ZMQ.Context context = ZMQ.context(1);

		// Se crea un thread
		final Thread zmqThread = new Thread() {
			
			@Override
			public void run() {
				
				try {
					// Se crea un socket REP
					ZMQ.Socket socket = context.socket(ZMQ.REP);
					
					// Se enlaza con el puerto 5555
					socket.bind("tcp://*:5555");
	
					// Mientras no se interrumpa el thread..
					while (!Thread.currentThread().isInterrupted()) {
						try {
							
							// Leeo el siguiente mensaje
							socket.recv(0);
							
						} catch (ZMQException e) {
							if (e.getErrorCode() == ZMQ.Error.ETERM.getCode()) {
								System.out.println("Interceptada la excepcion: " + e.toString());
								break;
							}
						}
					}
	
					socket.close();
				} catch (Throwable th) {
					System.out.println("Error no previsto (posiblemente no se ha posido hacer el binding): " + th.toString());
				}
			}
			
		};

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				System.out.println("W: interrupt received, killing server…");
				context.term();
				try {
					zmqThread.interrupt();
					zmqThread.join();
				} catch (InterruptedException e) {
				}
			}
		});

		zmqThread.start();
	}
}
