package zeromq.chapter2.multithreadadservice;

import org.zeromq.ZMQ;


public class RequestReplyClient {

    public static void main(String[] args) {
    	
    	long clientId = System.currentTimeMillis();
    	
        ZMQ.Context context = ZMQ.context(1);

        //  Socket to talk to server
        System.out.println("Connecting to hello world server…");

        ZMQ.Socket requester = context.socket(ZMQ.REQ);
        requester.connect("tcp://localhost:5555");

        for (int requestNbr = 0; requestNbr != 10; requestNbr++) {
        	
        	// Se prepara la peticion
            String request = String.format("REQUEST %d (client id %d)", requestNbr, clientId);
            System.out.println(String.format("Client > %s", request));
            
            // Se envia la peticion al server
            requester.send(request.getBytes(), 0);

            // Se procesa la respuesta
            String response = new String(requester.recv(0));
            System.out.println("Client < " + response);
        }
        requester.close();
        context.term();
    }
}