package zeromq.chapter1.requestreply;

import org.zeromq.ZMQ;

public class RequestReplyServer {
	
	  public static void main(String[] args) throws Exception {
		  
	        ZMQ.Context context = ZMQ.context(1);

	        //  Socket to talk to clients
	        ZMQ.Socket responder = context.socket(ZMQ.REP);
	        responder.bind("tcp://*:5555");

	        while (!Thread.currentThread().isInterrupted()) {
	        	
	            // Wait for next request from the client
	            String requestString = new String(responder.recv(0));

	            // Do some 'work'
	            Thread.sleep(1000);

	            // Send reply back to client
	            String reply = String.format("REPLY to request (%s)", requestString);
	            responder.send(reply.getBytes(), 0);
	        }
	        
	        responder.close();
	        context.term();
	        
	    }
	  
}
