package zeromq.chapter1.publishsubscribe;

import java.util.Random;

public class ZipCodeUtil {
	
	static Random random = new Random(System.currentTimeMillis());
	
	static String getZipCode() {
		int zipCodeInt = random.nextInt(3);
		return String.format("%05d", zipCodeInt);
	}
	
}
