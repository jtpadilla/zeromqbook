package zeromq.chapter1.version;

import org.zeromq.ZMQ;

public class Version {

	public static void main(String[] args) {
		System.out.println(String.format("Version string: %s, Version int: %d, Major: %d, Minor: %d, Patch: %d",
				ZMQ.getVersionString(), 
				ZMQ.getFullVersion(), 
				ZMQ.getMajorVersion(), 
				ZMQ.getMinorVersion(), 
				ZMQ.getPatchVersion()));
	}
	
}
