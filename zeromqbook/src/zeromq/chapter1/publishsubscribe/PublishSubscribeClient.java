package zeromq.chapter1.publishsubscribe;

import java.util.StringTokenizer;

import org.zeromq.ZMQ;

public class PublishSubscribeClient {

	public static void main(String[] args) {

		// Un zip code para este cliente
		String zipcodeFilter = ZipCodeUtil.getZipCode();

		// Se presenta el cliente
		System.out.println(String.format("Collecting updates from weather server from client zipcode %s", zipcodeFilter));

		// Se crea el contexto
		ZMQ.Context context = ZMQ.context(1);

		// El socket para hablar con el servidor
		ZMQ.Socket subscriber = context.socket(ZMQ.SUB);
		subscriber.connect("tcp://localhost:5556");

		// Subscribe to zipcode, default is NYC, 10001
		subscriber.subscribe(zipcodeFilter.getBytes());

		// Process 100 updates
		int update_nbr;
		long total_temp = 0;
		for (update_nbr = 0; update_nbr < 100; update_nbr++) {
			
			// Use trim to remove the tailing '0' character
			String string = subscriber.recvStr(0).trim();

			StringTokenizer sscanf = new StringTokenizer(string, " ");
			int zipcode = Integer.valueOf(sscanf.nextToken());
			int temperature = Integer.valueOf(sscanf.nextToken());
			int relhumidity = Integer.valueOf(sscanf.nextToken());
			
			// Se calcula la temperatura media
			total_temp += temperature;
			
			// Se muestra la atividad
			System.out.println(String.format("zipCode=%d, temperature=%d, relhumidity=%d, avgTemperature=%d", 
				zipcode, 
				temperature, 
				relhumidity, 
				update_nbr == 0 ? total_temp : (int) (total_temp / update_nbr)));

		}
		
		// Se muestra el resultado final
		System.out.println(String.format("Average temperature for zipcode '%s' was %d", zipcodeFilter, (int) (total_temp / update_nbr)));

		// Se cierra todo.
		subscriber.close();
		context.term();
	}
	
}