package zeromq.chapter1.publishsubscribe;

import java.util.Random;

import org.zeromq.ZMQ;

public class PublishSubscribeServer {
	
	  public static void main (String[] args) throws Exception {
		  
	        //  Prepare our context and publisher
	        ZMQ.Context context = ZMQ.context(1);

	        // Se crea el socket y se enlaza
	        ZMQ.Socket publisher = context.socket(ZMQ.PUB);
	        publisher.bind("tcp://*:5556");
	        publisher.bind("ipc://weather");

	        //  Initialize random number generator
	        Random srandom = new Random(System.currentTimeMillis());
	        
	        // Bucle del servidor
	        while (!Thread.currentThread ().isInterrupted ()) {
	        	
	            //  Get values that will fool the boss
	        	String zipcode = ZipCodeUtil.getZipCode();
	            int temperature = srandom.nextInt(215) - 80 + 1;
	            int relhumidity = srandom.nextInt(50) + 10 + 1;

	            // Send message to all subscribers
	            String update = String.format("%s %d %d", zipcode, temperature, relhumidity);
	            publisher.send(update, 0);
	        }

	        publisher.close ();
	        context.term ();
	    }
	  
}
