package zeromq.chapter1.divideandconquer;

import org.zeromq.ZMQ;

public class TaskWork {

	public static void main (String[] args) throws Exception {
		
		// Se obtiene el contexto
        ZMQ.Context context = ZMQ.context(1);

        //  Socket to receive messages on
        ZMQ.Socket receiver = context.socket(ZMQ.PULL);
        receiver.connect("tcp://localhost:5557");

        //  Socket to send messages to
        ZMQ.Socket sender = context.socket(ZMQ.PUSH);
        sender.connect("tcp://localhost:5558");

        //  Process tasks forever
        while (!Thread.currentThread ().isInterrupted ()) {
        	
        	// Se recibe el mensaje con los datos de la tarea
            String string = new String(receiver.recv(0)).trim();
            
            // Se extrae del mensaje la dureccion de esta
            long msec = Long.parseLong(string);
            
            //  Simple progress indicator for the viewer
            System.out.flush();
            System.out.print(string + '.');

            //  Do the work
            Thread.sleep(msec);

            //  Send results to sink
            sender.send("".getBytes(), 0);
            
        }
        
        // Nunca llegara aqui.
        sender.close();
        receiver.close();
        context.term();
        
    }
	
}
